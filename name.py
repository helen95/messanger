from twisted.internet.protocol import Factory
from twisted.internet import reactor
from twisted.internet.protocol import Protocol

class Echo(Protocol):
	def dataReceived(self, data):
		self.transport.write(data)
		
def main():		
	f = Factory()
	f.protocol = Echo
	reactor.listenTCP(8080, f)
	reactor.run()
	
if __name__ == '__main__':
    main()



