from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory 
from twisted.protocols.basic import LineOnlyReceiver 


SERV_IP="127.0.0.1"
port=8000

def check_in_dictionary(uin):
	for user in dict:
		if(user == uin):
			return "1"
	return "0"

class ChatProtocol(LineOnlyReceiver): 

    name = "" 

    def getName(self): 
        if self.name != "" : 
            return self.name 
        return self.transport.getPeer().host 

    def connectionMade(self): 
        print "New connection from " + self.getName() 
        self.sendLine("Welcome to my my chat server.") 
        self.sendLine("Send '/NAME [new name]' to change your name.") 
        self.sendLine("Send '/EXIT' to quit.") 
        self.factory.sendMessageToAllClients(self.getName() + " has joined the party.") 
        self.factory.clientProtocols.append(self)

    def connectionLost(self, reason): 
        print "Lost connection from " + self.getName() 
        self.factory.clientProtocols.remove(self) 
        self.factory.sendMessageToAllClients(self.getName() + " has disconnected.") 

    def lineReceived(self, line): 
        print self.getName() + " said " + line 
        if line[:5] == "/NAME": 
            oldName = self.getName() 
            self.name = line[5:].strip() 
            self.factory.sendMessageToAllClients(oldName + " changed name to " + self.getName()) 
        elif line == "/EXIT": 
            self.transport.loseConnection() 
        elif line == "/REG":
			self.
        else: 
            self.factory.sendMessageToAllClients(self.getName() + " says " + line) 

    def sendLine(self, line): 
        self.transport.write(line + "\r\n") 
        
    def handle_GETNAME(self, name):
	print "WOW , it works!"
	self.sendLine("hi, Kan")
	if name in self.users:
	    self.sendLine("Name taken, please choose another.")
	    return
	self.sendLine("Welcome, %s!" % (name,))
	self.name = name
	self.users[name] = self
	self.state = "CHAT"

    def handle_CHAT(self, message):
        message = "<%s> %s" % (self.name, message)
        for name, protocol in self.users.iteritems():
            if protocol != self:
		self.sendLine("hi")
		protocol.sendLine(message)


class ChatProtocolFactory(ServerFactory): 

    protocol = ChatProtocol 

    def __init__(self): 
        self.clientProtocols = [] 

    def sendMessageToAllClients(self, mesg): 
        for client in self.clientProtocols:
            client.sendLine(mesg) 


print "Starting Server"
factory = ChatProtocolFactory()
reactor.listenTCP(port, factory)
reactor.run()
